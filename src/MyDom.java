import org.w3c.dom.*;

import javax.xml.parsers.*;

import java.io.*;
import java.util.Iterator;

public class MyDom 
{
   public static void main(String[] args) throws Exception
   {

      
      	
         File inputFile = new File("a.txt");
         
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         
         Document doc=dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         
         System.out.println("Root Element = "+ doc.getDocumentElement().getNodeName());
         
    /*     
     * To print all first name in xml file 
     * 
     * NodeList nlist=doc.getElementsByTagName("firstname");
         
         for (int i = 0; i < nlist.getLength(); i++)
         {
        	 Node nnode=nlist.item(i);
        	 System.out.println("Current node : "+nnode.getNodeName());
        	 System.out.println("firstname : "+nnode.getTextContent());
        	 
		 }
      */
         
         NodeList nlist=doc.getElementsByTagName("student");
         
         for (int i = 0; i < nlist.getLength(); i++)
         {
        	 Node nnode =nlist.item(i);
         	 Element e=(Element)nnode;
        	 System.out.println("Student Roll no. : " + e.getAttribute("rollno"));
 			 System.out.println("First Name : " + e.getElementsByTagName("firstname").item(0).getTextContent());
 			 System.out.println("Last Name : " + e.getElementsByTagName("lastname").item(0).getTextContent());
 			 System.out.println("Nick Name : " + e.getElementsByTagName("nickname").item(0).getTextContent());
 		     System.out.println("Marks : " + e.getElementsByTagName("marks").item(0).getTextContent());
 	         System.out.println("Marks : " + e.getElementsByTagName("marks").item(1).getTextContent());
		 }
         
        
   }
}

