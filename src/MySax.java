import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class MySax extends DefaultHandler
{
	boolean bfirstname=false;
	boolean blastname=false;
	boolean bmarks=false;
	boolean bnicknames=false;
	
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		if(qName.equalsIgnoreCase("student"))
		{
			System.out.println("Rollno: "+attributes.getValue("rollno"));
		}
		else if (qName.equalsIgnoreCase("firstname"))
		{
			bfirstname=true;
		}
		else if (qName.equalsIgnoreCase("lastname"))
		{
			blastname=true;
		}
		else if (qName.equalsIgnoreCase("nickname"))
		{
			bnicknames=true;
		}
		else if (qName.equalsIgnoreCase("marks"))
		{
			bmarks=true;
		}
	}
	
	/*@Override
	public void endElement(String uri, String localName, String qName) throws SAXException 
	{
	
	}*/
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException 
	{
		if(bfirstname)
		{
			System.out.println("First name:: "+new String(ch,start,length));
			bfirstname=false;
		}
		else if (blastname) 
		{
			System.out.println("Last name:: "+new String(ch,start,length));
			blastname=false;
		}
		else if (bnicknames) 
		{
			System.out.println("Nick name:: "+new String(ch,start,length));
			bnicknames=false;
		}
		else if (bmarks) 
		{
			System.out.println("Marks:: "+new String(ch,start,length));
			bmarks=false;
		}
	}
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException 
	{
		File inputFile = new File("a.txt");
		
		SAXParserFactory saxpf=SAXParserFactory.newInstance();
		SAXParser saxp=saxpf.newSAXParser();
		
		saxp.parse(inputFile, new MySax());
		
		
	}

}
